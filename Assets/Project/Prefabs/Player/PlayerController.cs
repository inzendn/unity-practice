﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // INSPECTOR VARIABLES
    public float MoveSpeed;
    public float DashSpeed;
    public float DashTime;
    public float JumpForce;
    public float ShotForce;

    // PLAYER COMPONENTS
    Rigidbody2D PlayerRigidBody;
    BoxCollider2D PlayerCollider;
    public Animator PlayerAnimator;
    ParticleSystem PlayerParticle;
    AudioSource PlayerAudioSource;

    // PLAYER DIMENSIONS
    float Width;
    float Height;

    // PLAYER ATTACKS
    public GameObject Pellets;
    public GameObject ChargedShot;
    public GameObject ChargedBlast;
    Vector2 PelletVector;
    Vector3 PelletPosition;
    int ChargeState = 0;
    Coroutine ChargeRoutine;

    // PLAYER AUDIO
    Coroutine ChargeFXRoutine;
    public AudioClip StartChargeSFX;
    public AudioClip ChargingSFX;

    // PLAYER PARTICLES
    ParticleSystem.MainModule setting;
    ParticleSystem.TrailModule trails;

    // PLAYER MISCELLANEOUS
    public GameObject container;
    bool FacingRight;
    bool IsDashing;
    bool IsWallJumping;
    float WallFrictionLerp = 0f;
    Coroutine DashRoutine;

    // RAYCASTS
    RaycastHit2D DownHit;
    RaycastHit2D RightHit;
    RaycastHit2D LeftHit;    

    // Start is called before the first frame update
    void Start()
    {
        PlayerRigidBody = gameObject.GetComponent<Rigidbody2D>();
        Width = gameObject.GetComponent<BoxCollider2D>().size.x;
        Height = gameObject.GetComponent<BoxCollider2D>().size.y;
        PlayerParticle = gameObject.GetComponent<ParticleSystem>();
        PlayerAudioSource = gameObject.GetComponent<AudioSource>();
        setting = PlayerParticle.main;
        trails = PlayerParticle.trails;
        PlayerParticle.Stop();
        FacingRight = true;
        IsDashing = false;
    }

    // Update is called once per frame
    void Update()
    {        
        // DOWN HIT
        DownHit = Physics2D.Raycast(
            new Vector2(transform.position.x, transform.position.y - (Height / 2) - 0.059f),
            Vector2.down,
            0.1f
        );
        // RIGHT HIT
        RightHit = Physics2D.Raycast(
            new Vector2(transform.position.x + (Width / 2) + 0.2f, transform.position.y - 1),
            Vector2.right,
            0.1f
        );
        // LEFT HIT
        LeftHit = Physics2D.Raycast(
            new Vector2(transform.position.x - (Width / 2) - 0.2f, transform.position.y - 1),
            Vector2.left,
            0.1f
        );

        // DO TOP HIT FOR HELMET SMAAAASH
        
        Move();
        AnimateMove();
        Dash();
        FaceDirection();
        Shoot();
    }

    void LateUpdate()
    {
        Jump();
              
    }

    #region Face Direction
    void FaceDirection()
    {
        if(FacingRight)
        {
            container.transform.localScale = new Vector2(1,1);
            PelletVector = new Vector2(
                ShotForce,
                0
            );
            PelletPosition = new Vector2(transform.position.x + 1.2f,
                transform.position.y - 0.5f);
        }
        else if(!FacingRight)
        {
            container.transform.localScale = new Vector2(-1, 1);
            PelletVector = new Vector2(
                -ShotForce,
                0
            );
            PelletPosition = new Vector2(transform.position.x - 1.2f,
                transform.position.y - 0.5f);
        }
        
    }
    #endregion
    
    #region Move
    void Move()
    {
        if(!IsDashing && !IsWallJumping)
        {         
            if(Input.GetKey(KeyCode.RightArrow))
            {
                if(DownHit.collider != null || PlayerRigidBody.velocity.x == 0)
                    PlayerRigidBody.velocity = new Vector2(MoveSpeed, PlayerRigidBody.velocity.y);
                else if(PlayerRigidBody.velocity.x < MoveSpeed)
                    PlayerRigidBody.velocity = new Vector2(-PlayerRigidBody.velocity.x, PlayerRigidBody.velocity.y);

                if(RightHit.collider?.tag == "Wall" && PlayerRigidBody.velocity.y < -1.5)
                {
                    if(PlayerRigidBody.velocity.y == -3.981f)
                    {
                        FacingRight = false;
                        PlayerAnimator.SetTrigger("TriggerWallslide");
                    }

                    PlayerRigidBody.velocity = new Vector2(PlayerRigidBody.velocity.x, Mathf.Lerp(2, -3f, WallFrictionLerp += 0.1f));
                }
                else
                    FacingRight = true;
            }
            else if(Input.GetKey(KeyCode.LeftArrow))
            {
                if(DownHit.collider != null || PlayerRigidBody.velocity.x == 0)
                    PlayerRigidBody.velocity = new Vector2(-MoveSpeed, PlayerRigidBody.velocity.y);
                else if(PlayerRigidBody.velocity.x > -MoveSpeed)
                    PlayerRigidBody.velocity = new Vector2(-PlayerRigidBody.velocity.x, PlayerRigidBody.velocity.y);

                if(LeftHit.collider?.tag == "Wall" && PlayerRigidBody.velocity.y < -1.5)
                {
                    if(PlayerRigidBody.velocity.y == -3.981f)
                    {
                        FacingRight = true;
                        PlayerAnimator.SetTrigger("TriggerWallslide");
                    }
                        
                    PlayerRigidBody.velocity = new Vector2(PlayerRigidBody.velocity.x, Mathf.Lerp(2, -3f, WallFrictionLerp += 0.1f));
                }
                else
                    FacingRight = false;
            }
            else if(Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
            {
                PlayerRigidBody.velocity = new Vector2(0, PlayerRigidBody.velocity.y);
                PlayerAnimator.ResetTrigger("TriggerDash");
            }
        }            
    }

    void AnimateMove()
    {
        if(PlayerRigidBody.velocity.x == MoveSpeed || PlayerRigidBody.velocity.x == -MoveSpeed)
        {
            PlayerAnimator.SetTrigger("TriggerRunning");
            PlayerAnimator.ResetTrigger("TriggerIdle");
            PlayerAnimator.ResetTrigger("TriggerDash");

        }
        else if(PlayerRigidBody.velocity.x == 0)
        {
            PlayerAnimator.SetTrigger("TriggerIdle");
            PlayerAnimator.ResetTrigger("TriggerRunning");
            PlayerAnimator.ResetTrigger("TriggerDash");
        }
    }
    #endregion

    #region Dash
    void Dash()
    {
        if(Input.GetKeyDown(KeyCode.C) && DownHit.collider != null)
        {
            DashRoutine = StartCoroutine(DashGenerator());
            PlayerAnimator.SetTrigger("TriggerDash");
            PlayerAnimator.ResetTrigger("TriggerRunning");
            PlayerAnimator.ResetTrigger("TriggerIdle");
        }
        else if(Input.GetKeyUp(KeyCode.C))
        {
            StopCoroutine(DashRoutine);
            IsDashing = false;
            if(Input.GetAxis("Horizontal") == 0)
                PlayerRigidBody.velocity = new Vector2(0, PlayerRigidBody.velocity.y);
        }
    }

    IEnumerator DashGenerator()
    {
        IsDashing = true;
        if(FacingRight)
        {
            PlayerRigidBody.velocity = (new Vector2(
                DashSpeed,
                PlayerRigidBody.velocity.y
            ));
        }
        else if(!FacingRight)
        {
            PlayerRigidBody.velocity = (new Vector2(
                -DashSpeed,
                PlayerRigidBody.velocity.y
            ));
        }
        yield return new WaitForSecondsRealtime(DashTime);
        if(Input.GetAxis("Horizontal") == 0)
            PlayerRigidBody.velocity = new Vector2(0, PlayerRigidBody.velocity.y);
        IsDashing = false;
    }
    #endregion

    #region Jump/Wall Climb
    void Jump()
    {
        if(Input.GetKeyDown(KeyCode.X))
        {
            WallFrictionLerp = 0; // INTERPOLATES FRICTION

            // UPWARD JUMP
            if(DownHit.collider?.tag == "Floor")
            {
                PlayerRigidBody.AddForce(new Vector2(0, JumpForce));
                PlayerAnimator.SetTrigger("TriggerJump");
                PlayerAnimator.ResetTrigger("TriggerIdle");
                PlayerAnimator.ResetTrigger("TriggerLanding");
                PlayerAnimator.ResetTrigger("TriggerFalling");
                PlayerAnimator.ResetTrigger("TriggerWallslide");
                PlayerAnimator.ResetTrigger("TriggerRunning");                
            }
            // WALL JUMP
            else if(
                (Input.GetKey(KeyCode.RightArrow) && RightHit.collider?.tag == "Wall") 
                || (Input.GetKey(KeyCode.LeftArrow) && LeftHit.collider?.tag == "Wall")
            ) {
                StopCoroutine(WallJumpGenerator());
                StartCoroutine(WallJumpGenerator());
            }
            else if(
                (Input.GetKey(KeyCode.LeftArrow) && RightHit.collider?.tag == "Wall")
                || (Input.GetKey(KeyCode.RightArrow) && LeftHit.collider?.tag == "Wall")
            ) {
                PlayerAnimator.SetTrigger("TriggerJump");
                PlayerAnimator.ResetTrigger("TriggerWallslide");
                if(FacingRight)
                    PlayerRigidBody.AddForce(new Vector2(200, JumpForce));
                else if(!FacingRight)
                    PlayerRigidBody.AddForce(new Vector2(-200, JumpForce));
            }
            // WALL JUMP W/O LEFT/RIGHT ARROW
            else
            {
                if(RightHit.collider?.tag == "Wall")
                {
                    PlayerAnimator.SetTrigger("TriggerWallclimb");
                    PlayerRigidBody.AddForce(new Vector2(-200, JumpForce));
                }
                else if(LeftHit.collider?.tag == "Wall")
                {
                    PlayerAnimator.SetTrigger("TriggerWallclimb");
                    PlayerRigidBody.AddForce(new Vector2(200, JumpForce));
                }
            }
        }

        // TRIGGER FALLING ANIMATION
        if(PlayerRigidBody.velocity.y < -1f)
        {
            if(RightHit.collider?.tag != "Wall" && LeftHit.collider?.tag != "Wall")
                PlayerAnimator.SetTrigger("TriggerFalling");
            else if((RightHit.collider?.tag == "Wall" || LeftHit.collider?.tag == "Wall") && (Mathf.Abs(Input.GetAxis("Horizontal")) == 0)) {
                PlayerAnimator.SetTrigger("TriggerFalling");
            }
        }
    }

    IEnumerator WallJumpGenerator()
    {
        IsWallJumping = true;

        if(FacingRight)
        {
            PlayerRigidBody.velocity = Vector2.zero;
            PlayerRigidBody.AddForce(new Vector2(-300, JumpForce * 0.8f));            
        }
        else if(!FacingRight)
        {
            PlayerRigidBody.velocity = Vector2.zero;
            PlayerRigidBody.AddForce(new Vector2(300, JumpForce * 0.8f));
        }

        PlayerAnimator.SetTrigger("TriggerWallclimb");
        PlayerAnimator.ResetTrigger("TriggerWallslide");
        PlayerAnimator.ResetTrigger("TriggerRunning");
        PlayerAnimator.ResetTrigger("TriggerLanding");
        PlayerAnimator.ResetTrigger("TriggerFalling");

        yield return new WaitForSecondsRealtime(0.15f);

        IsWallJumping = false;
    }

    #endregion

    #region Shoot
    void Shoot()
    {
        if(Input.GetKeyUp(KeyCode.Z))
        {            
            StopCoroutine(ChargeRoutine);
            StopCoroutine(ChargeFXRoutine);
            PlayerAudioSource.Stop();
            PlayerParticle.Stop();

            if(ChargeState == 1)
            {
                PlayerAnimator.SetTrigger("TriggerShooting");
                GameObject instance = Instantiate(
                    ChargedShot,
                    PelletPosition,
                    Quaternion.identity,
                    transform.parent
                );
                Rigidbody2D missile = instance.GetComponent<Rigidbody2D>();
                missile.AddForce(PelletVector * 1f);
            }                
            else if(ChargeState == 2)
            {
                PlayerAnimator.SetTrigger("TriggerShooting");
                GameObject instance = Instantiate(
                    ChargedBlast,
                    PelletPosition,
                    Quaternion.identity,
                    transform.parent
                );
                Rigidbody2D missile = instance.GetComponent<Rigidbody2D>();
                missile.AddForce(PelletVector * 1.25f);
            }
            
            ChargeState = 0;
        }
        else if(Input.GetKeyDown(KeyCode.Z))
        {            
            ChargeRoutine = StartCoroutine(ChargeShot());
            ChargeFXRoutine = StartCoroutine(ChargeSoundFX());

            PlayerAnimator.SetTrigger("TriggerShooting");

            GameObject instance = Instantiate(
                Pellets,
                PelletPosition,
                Quaternion.identity,
                transform.parent
            );

            Rigidbody2D missile = instance.GetComponent<Rigidbody2D>();
            missile.AddForce(PelletVector);
        }
    }

    IEnumerator ChargeShot()
    {        
        setting.startColor = new ParticleSystem.MinMaxGradient(Color.blue, Color.white);

        yield return new WaitForSecondsRealtime(0.5f);
        trails.mode = ParticleSystemTrailMode.PerParticle;
        PlayerParticle.Play();
        ChargeState = 1;
        Debug.Log(ChargeState);

        yield return new WaitForSecondsRealtime(1.5f);
        setting.startColor = new ParticleSystem.MinMaxGradient(Color.yellow, Color.white);
        trails.mode = ParticleSystemTrailMode.Ribbon;
        trails.ribbonCount = 1;
        ChargeState = 2;
        Debug.Log(ChargeState);
    }

    IEnumerator ChargeSoundFX()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        PlayerAudioSource.clip = StartChargeSFX;
        PlayerAudioSource.Play();
        yield return new WaitForSecondsRealtime(StartChargeSFX.length);
        PlayerAudioSource.loop = true;
        PlayerAudioSource.clip = ChargingSFX;
        PlayerAudioSource.Play();
    }
    #endregion    
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        // LANDING
        if(collision.collider?.tag == "Floor")
        {
            PlayerAnimator.SetTrigger("TriggerLanding");
            PlayerAnimator.ResetTrigger("TriggerJump");
            PlayerAnimator.ResetTrigger("TriggerWallslide");
        }
    }
}
