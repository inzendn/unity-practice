﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Movement")]
    public float walkingSpeed;
    [Header("Jump")]
    public float jumpingSpeed;

    private Rigidbody2D playerRigidBody;
    private float width;
    private float height;
    private BoxCollider2D playerCollision;
    public PhysicsMaterial2D physicsMaterial;

    private Animator playerAnimator;

    public GameObject container;
    public GameObject pellets;

    private Vector2 pelletPosition;
    private Vector2 pelletVector;

    private bool facingRight;
    private bool canDash;

    // Start is called before the first frame update
    void Start()
    {
        playerRigidBody = gameObject.GetComponent<Rigidbody2D>();
        playerCollision = gameObject.GetComponent<BoxCollider2D>();
        physicsMaterial = gameObject.GetComponent<BoxCollider2D>().sharedMaterial;
        playerAnimator = gameObject.GetComponent<Animator>();
        width = gameObject.GetComponent<BoxCollider2D>().size.x;
        height = gameObject.GetComponent<BoxCollider2D>().size.y;
        canDash = true;
    }

    // Update is called once per frame
    void Update()
    {
        playerAnimator.SetBool("IsJumping", playerRigidBody.velocity.y > 0);
        playerAnimator.SetBool("IsFalling", playerRigidBody.velocity.y < 0);
        playerAnimator.SetBool("IsRunning", 0.3 < Math.Abs(playerRigidBody.velocity.x));

        RaycastHit2D hit = Physics2D.Raycast(
            new Vector2(transform.position.x, transform.position.y - (height / 2) - 0.5f),
            Vector2.down,
            0.1f
        );
        RaycastHit2D righthit = Physics2D.Raycast(
            new Vector2(transform.position.x + (width / 2), transform.position.y - 1),
            Vector2.right,
            0.1f
        );
        RaycastHit2D lefthit = Physics2D.Raycast(
            new Vector2(transform.position.x - (width / 2), transform.position.y - 1),
            Vector2.left,
            0.1f
        );

        // LEFT/RIGHT Facing
        Facing(facingRight);            

        // Player Move
        if(!Input.GetKey(KeyCode.C))
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            playerRigidBody.velocity = new Vector2
            (                
                -1 * walkingSpeed,
                playerRigidBody.velocity.y
            );
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            playerRigidBody.velocity = new Vector2
            (
                1f * walkingSpeed,
                playerRigidBody.velocity.y
            );
        }
        else if (hit.collider != null)
        {
            playerRigidBody.velocity = new Vector2
            (
                0 * walkingSpeed,
                playerRigidBody.velocity.y
            );
        }

        // Player Facing
        if(Input.GetAxis("Horizontal") > 0)
        {
            if(righthit.collider?.tag == "Wall" && playerRigidBody.velocity.y < -1)
                facingRight = false;
            else
                facingRight = true;
        }
        else if(Input.GetAxis("Horizontal") < 0)
        {
            if(lefthit.collider?.tag == "Wall" && playerRigidBody.velocity.y < -1)
                facingRight = true;
            else
                facingRight = false;
        }

        // Player Jump
        if(Input.GetKeyDown(KeyCode.X))
        {
            if(hit.collider != null)
            {
                playerRigidBody.AddForce(new Vector2(0, 800));
                playerAnimator.ResetTrigger("IsLanding");
            }
            else if(righthit.collider?.tag == "Wall")
            {
                playerRigidBody.velocity = new Vector2(-45, 13);
                playerAnimator.ResetTrigger("IsLanding");
            }
            else if(lefthit.collider?.tag == "Wall")
            {
                playerRigidBody.velocity = new Vector2(45, 13);
                playerAnimator.ResetTrigger("IsLanding");
            }
        }
                       
        // Player Pee
        if(Input.GetKeyDown(KeyCode.Z))
        {
            playerAnimator.SetBool("IsRunning", false);
            StopAllCoroutines();
            StartCoroutine(XShooting());

            GameObject instance = Instantiate(
                pellets,
                pelletPosition,
                Quaternion.identity,
                transform.parent
            );

            Rigidbody2D missile = instance.GetComponent<Rigidbody2D>();
            missile.AddForce(pelletVector);
        }
    }

    public void Facing(bool facingRight)
    {
        if(facingRight)
        {
            container.transform.localScale = new Vector2(1, 1);
            pelletVector = new Vector2(
                2000f,
                0
            );
            pelletPosition = new Vector2(transform.position.x + 1.2f,
                transform.position.y - 0.5f);
        }
        else if(!facingRight)
        {
            container.transform.localScale = new Vector2(-1, 1);
            pelletVector = new Vector2(
                -2000f,
                0
            );
            pelletPosition = new Vector2(transform.position.x - 1.2f,
                transform.position.y - 0.5f);
        }
    }

    IEnumerator XShooting()
    {
        playerAnimator.SetBool("IsShooting", true);
        playerAnimator.SetTrigger("TriggerShooting");
        yield return new WaitForSecondsRealtime(0.5f);

        playerAnimator.SetBool("IsShooting", false);
        playerAnimator.ResetTrigger("TriggerShooting");
    }

    void OnCollisionEnter2D (Collision2D collision)
    {
        Debug.Log(collision.collider.tag);
        if(collision.gameObject.tag == "Floor")
            playerAnimator.SetTrigger("IsLanding");
    }
}
