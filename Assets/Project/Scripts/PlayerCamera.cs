﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public GameObject player;
    private float width;
    private float height;
    private bool moveCamera;
    public RaycastHit2D hit;
    public RaycastHit2D righthit;

    // Start is called before the first frame update
    void Start()
    {
        moveCamera = true;
        width = player.gameObject.GetComponent<BoxCollider2D>().size.x;
        height = player.gameObject.GetComponent<BoxCollider2D>().size.y;
    }

    // Update is called once per frame
    void Update()
    {
        righthit = Physics2D.Raycast(
            new Vector2(player.transform.position.x + (width / 2), player.transform.position.y),
            Vector2.right,
            0.1f
        );
        hit = Physics2D.Raycast(
            new Vector2(player.transform.position.x, player.transform.position.y - (height / 2) -1f),
            Vector2.down,
            0.1f
        );

        if(righthit.collider?.tag == "Wall")
        {
            transform.position = new Vector3(
                transform.position.x,
                transform.position.y,
                transform.position.z
            );
        }
        if(hit.collider?.tag == "Floor" || hit.collider == null)
        {
            transform.position = new Vector3(
                Mathf.Lerp(transform.position.x, player.transform.position.x, 4 * Time.deltaTime),
                transform.position.y,
                transform.position.z
            );
        }
        //if(player.GetComponent<Rigidbody2D>().velocity.y < -0)
        //{
        //    transform.position = new Vector3(
        //        Mathf.Lerp(transform.position.x, player.transform.position.x, 4 * Time.deltaTime),
        //        Mathf.Lerp(transform.position.y, player.transform.position.y + 4.2f, 200 * Time.deltaTime),
        //        transform.position.z
        //    );
        //}
    }
}
