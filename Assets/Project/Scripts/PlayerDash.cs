﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDash : MonoBehaviour
{
    private Rigidbody2D rb;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private int direction;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(10f, rb.velocity.y);
        direction = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            direction = 1;
        }
        else if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            direction = 0;
        }

        if(Input.GetKey(KeyCode.C))
        {
            dashTime -= Time.deltaTime;

            if(dashTime > 0)
            {
                if(direction == 0)
                {
                    rb.velocity = new Vector2(
                        1f * dashSpeed, 
                        rb.velocity.y
                    );
                }
                else if(direction == 1)
                {
                    rb.AddForce(new Vector2(
                        -1f * dashSpeed,
                        rb.velocity.y
                    ));
                }
            }
        }
        else if(!Input.GetKeyDown(KeyCode.C))
        {
            dashTime = startDashTime;
        }
    }
}
