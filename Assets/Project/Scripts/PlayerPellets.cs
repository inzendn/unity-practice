﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPellets : MonoBehaviour
{
    public float force;

    private Rigidbody2D missileRigidbody2D;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 0.65f);
        missileRigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
